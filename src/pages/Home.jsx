import React from "react";
import "./Home.css";

function ShowAlert() {
  document.getElementById("yell").innerHTML = "<h1>Stopppp!!!<h1>";
  document.getElementById("my-avatar").className = "shake-crazy";
}

function StopAlert() {
  document.getElementById("yell").innerHTML = "";
  document.getElementById("my-avatar").className = "";
}

function Home() {
  return (
    <>
      {/* Header section */}
      <div id="top" />
      <div className="heading-container">
        <div className="title-container">
          <h1 className="name">
            <div id="text">
              THINH LE
              <div id="type" />
            </div>
          </h1>
          <h3 className="job">
            <div id="text2">
              Front End Developer
              <div id="type2" />
            </div>
          </h3>
        </div>
      </div>
      {/* End header section */}

      {/* About section */}
      <div id="about" />
      <div className="section-bar">
        <div className="title-bar">ABOUT ME</div>
      </div>
      <div className="about-section">
        <div className="content-box-about">
          <div
            id="my-avatar"
            role="presentation"
            className="shake"
            onMouseDown={ShowAlert}
            onMouseUp={StopAlert}
          />

          <div className="content-about">
            <div id="yell" className="shake-crazy shake-constant" />
            <p>
              I have gained IT experience through my job with Accredo Packing,
              Inc. as an ERP Support Specialist, and my internship with EDGE 196
              (Front-end Developer). Currently, I am focusing on my final year
              at University of Houston in order to get my Bachelor Degree.
            </p>
            <p>
              Please feel free to contact with me if you would like to learn
              more about me, and how I could contribute to your company.
            </p>
          </div>
        </div>
      </div>
      {/* End about section */}

      {/* Work section */}
      <div id="works" />
      <div className="section-bar">
        <div className="title-bar">WORK HISTORY</div>
      </div>

      <div className="work-section">
        <div className="content-box-work">
          <div id="accredo" className="shake" />
          <div className="content-work">
            <h2>Administrative Assistant</h2>
            <h4>(4/2020 - 3/2021)</h4>
            <h4>Accredo Packing, Inc. - Full time</h4>
            <ul>
              <li>Translate messages from Converting Management.</li>
              <li>Monitor training packets.</li>
              <li>HR-realated projects.</li>
              <li>Coordinate rework jobs in Production.</li>
            </ul>
          </div>
        </div>
      </div>

      <div className="work-section">
        <div className="content-box-work">
          <div id="accredo" className="shake" />
          <div className="content-work">
            <h2>ERP Support Specialist</h2>
            <h4>6/2019 - 4/2020</h4>
            <h4>Accredo Packing, Inc. - Full time</h4>
            <ul>
              <li>
                Troubleshooting and resolving IT issues in a timely manner.
              </li>
              <li>Communicating with coworkers to diagnose problems.</li>
              <li>Providing support remotely when necessary.</li>
              <li>Installing and configuring hardware and software.</li>
            </ul>
          </div>
        </div>
      </div>
      {/* End work section */}

      {/* Education section */}
      <div id="education" />
      <div className="section-bar">
        <div className="title-bar">EDUCATION</div>
      </div>

      <div className="education-section">
        <div className="content-box-edu">
          <div id="uhcl" className="shake" />
          <div className="content-edu">
            <h1>2020 - 2022</h1>
            <h3>
              <u>University of Houston</u>
            </h3>
            <h3>
              <u>Clear Lake</u>
            </h3>
            <p>
              <i>Bachelor of Science</i>
            </p>
            <p>
              <i>Computer Science</i>
            </p>
          </div>
        </div>

        <div className="content-box-edu">
          <div id="sanjac" className="shake" />
          <div className="content-edu">
            <h1>2017 - 2020</h1>
            <h3>
              <u>San Jacinto College</u>
            </h3>
            <h3>
              <u>South Campus</u>
            </h3>
            <p>
              <i>Associate of Science</i>
            </p>
            <p>
              <i>Computer Science</i>
            </p>
          </div>
        </div>

        <div className="content-box-edu">
          <div id="uit" className="shake" />
          <div className="content-edu">
            <h1>2013 - 2015</h1>
            <h3>
              <a href="www.google.com">University of IT (VNU)</a>
            </h3>
            <h3>
              <u>Ho Chi Minh City</u>
            </h3>
            <p>
              <i>Bachelor of Science</i>
            </p>
            <p>
              <i>Software Engineering</i>
            </p>
          </div>
        </div>
      </div>
      {/* End education section */}

      {/* Skills section */}
      <div id="skills" />
      <div className="section-bar">
        <div className="title-bar">SKILLS</div>
      </div>

      <div className="skills-section">
        <div className="content-box-skills">
          <div className="content-skills">Hahaha</div>
        </div>
      </div>
      {/* End skills section */}
    </>
  );
}

export default Home;

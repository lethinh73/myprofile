import React from 'react';
import './Navbar.css';

function Navbar() {
  return (
    <nav className="navbar fixed-top navbar-expand-md navbar-dark bg-dark">
      <a className="navbar-brand" href="#top">
        Thinh Le
      </a>
      <ul className="navbar-nav">
        <li className="nav-item">
          <a className="nav-link" href="#top">
            Home
          </a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#about">
            About
          </a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#works">
            Works
          </a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#education">
            Education
          </a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#contact">
            Contact
          </a>
        </li>
      </ul>
    </nav>
  );
}

export default Navbar;
